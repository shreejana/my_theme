/**
* Configuration.
*
* Project Configuration for gulp tasks.
*
* Edit the variables as per your project requirements.
*/

/**
 * Load Plugins.
 *
 * Load gulp plugins and assing them semantic names.
 */
var gulp         = require('gulp'); // Gulp of-course

// CSS related plugins.
var sass         = require('gulp-sass'); // Gulp pluign for Sass compilation
var imagemin     = require( 'gulp-imagemin' ); // Minifies Images.
var zip			 = require( 'gulp-zip' );



gulp.task('copy', function() {
  gulp.src('index.html')
  .pipe(gulp.dest('assets'))
});

// Compiles SCSS into CSS.
gulp.task('sass', function() {
  gulp.src('styles/main.scss')
  .pipe(sass({style: 'expanded'}))
      .pipe(gulp.dest('assets'))
});

// Minfies image files.
gulp.task('images', function() {
  return gulp.src('assets/img/*')
    .pipe(imagemin({optimizationLevel: 7, progressive: true}))
    .pipe(gulp.dest('assets/miniimg'));
});

gulp.task('zip', function() {
  return gulp.src('assets/**')
    .pipe(zip('assets.zip'))
        .pipe(gulp.dest('./zip'));
});

