<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package my_theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" style="<?php echo $css ?>">

		<?php
		while ( have_posts() ) :
			the_post();

			
			$css = rwmb_the_value( 'background', '', '', false );
			get_template_part( 'template-parts/content', get_post_type() );

			$post_id= get_the_ID();
			echo get_post_meta( $post_id, "custom_metatext_1", true);
			echo get_post_meta( $post_id, "custom_metacolor_4", true);
			echo get_post_meta( $post_id, "custom_metamap_6", true);
			$meta_image= get_post_meta( $post_id, "custom_metaimage_advanced_8", true);
			?>
			<img src =" <?php echo $meta_image[ 'custom_metaimage_advanced_8' ];?>">

			<?php 
			rwmb_the_value( 'custom_metaimage_advanced_8', array( 'size' => 'thumbnail' ) );
			rwmb_the_value( 'custom_metacolor_4' );
			rwmb_the_value( 'custom_metamap_6' );

			
			echo '<div id="page" style="', $css, '"></div>';

			// $images = rwmb_meta( 'info', array( 'size' => 'thumbnail' ) );
			// foreach ( $images as $image ) {
			//     echo '<a href="', $image['full_url'], '"><img src="', $image['url'], '"></a>';
			// 	}

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
