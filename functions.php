<?php
/**
 * my_theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package my_theme
 */

if ( ! function_exists( 'my_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function my_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on my_theme, use a find and replace
		 * to change 'my_theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'my_theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'my_theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'my_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'my_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function my_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'my_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'my_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function my_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'my_theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'my_theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'my_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function my_theme_scripts() {
	wp_enqueue_style( 'my_theme-style', get_stylesheet_uri() );

	wp_enqueue_script( 'my_theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'my_theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'my_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


//MetaBox
function custom_meta_box_markup( $object ) {

    wp_nonce_field( 'custom_meta_nonce_action', 'meta-box-nonce' );
    ?>
    <div>
        <label for="meta-box-text">Text</label>
        <input name="meta-box-text" type="text" value="<?php echo get_post_meta( $object->ID, "meta-box-text", true ); ?>">
        <br>
        <label for="meta-box-dropdown">Dropdown</label>
        <select name="meta-box-dropdown">
	        <?php 
	            $option_values = array(1, 2, 3);

	            foreach( $option_values as $key => $value ) 
	            {
	                if( $value == get_post_meta( $object->ID, "meta-box-dropdown", true ) )
	                {
	                    ?>
	                        <option selected><?php echo $value; ?></option>
	                    <?php    
	                }
	                else
	                {
	                    ?>
	                        <option><?php echo $value; ?></option>
	                    <?php
	                }
	            }
            ?>
        </select>

        <br>

        <label for="meta-box-checkbox">Check Box</label>
        <?php
            $checkbox_value = get_post_meta( $object->ID, "meta-box-checkbox", true );
            if( $checkbox_value == "" )
            {
                ?>
                    <input name="meta-box-checkbox" type="checkbox" value="true">
                <?php
            }
            else if( $checkbox_value == "true" )
            {
                ?>  
                    <input name="meta-box-checkbox" type="checkbox" value="true" checked>
                <?php
            }
        ?>
    </div>
    <?php  
}

function wpdocs_register_meta_boxes() {

    add_meta_box( 'meta-box-id', __( 'My Meta Box', 'my_theme' ), 'custom_meta_box_markup', 'post' );
}

add_action( 'add_meta_boxes', 'wpdocs_register_meta_boxes' );


function save_custom_meta_box( $post_id, $post, $update ) {

    if ( !isset( $_POST[ "meta-box-nonce" ] ) || !wp_verify_nonce($_POST[ "meta-box-nonce" ],'custom_meta_nonce_action' ))
        return $post_id;

    if( defined( "DOING_AUTOSAVE" ) && DOING_AUTOSAVE )
        return $post_id;

    $slug = "post";
    if( $slug != $post->post_type )
        return $post_id;

    $meta_box_text_value = "";
    $meta_box_dropdown_value = "";
    $meta_box_checkbox_value = "";

    if( isset( $_POST[ "meta-box-text" ] ) )
    {
        $meta_box_text_value = $_POST[ "meta-box-text" ];
    }   
    update_post_meta( $post_id, "meta-box-text", $meta_box_text_value );

    if( isset( $_POST[ "meta-box-dropdown" ] ) )
    {
        $meta_box_dropdown_value = $_POST[ "meta-box-dropdown" ];
    }   
    update_post_meta( $post_id, "meta-box-dropdown", $meta_box_dropdown_value );

    if( isset( $_POST[ "meta-box-checkbox" ] ) )
    { 
        $meta_box_checkbox_value = $_POST[ "meta-box-checkbox" ];
    }   
    update_post_meta( $post_id, "meta-box-checkbox", $meta_box_checkbox_value );
}

add_action( "save_post", "save_custom_meta_box", 10, 3 );


//From MetaBox Plugin generated code
function custom_meta_box( $meta_boxes ) {

	$prefix = 'custom_meta';
	$meta_boxes[] = array(
		'id' => 'custom_meta_box_id',
		'title' => esc_html__( 'My Custom Meta Box', 'my_theme' ),
		'post_types' => array('post', 'page' ),
		'context' => 'side',
		'priority' => 'default',
		'autosave' => 'true',
		'fields' => array(
			array(
				'id' => $prefix . 'text_1',
				'type' => 'text',
				'name' => esc_html__( 'Text', 'my_theme' ),
			),
			array(
				'id' => $prefix . 'checkbox_2',
				'name' => esc_html__( 'Checkbox', 'my_theme' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'Default Description', 'my_theme' ),
			),
			array(
				'id' => $prefix . 'button_3',
				'type' => 'button',
				'name' => esc_html__( 'Button', 'my_theme' ),
			),
			array(
				'id' => $prefix . 'color_4',
				'name' => esc_html__( 'Color Picker', 'my_theme' ),
				'type' => 'color',
			),
			array(
				'id' => $prefix . 'datetime_5',
				'type' => 'datetime',
				'name' => esc_html__( 'Date Time Picker', 'my_theme' ),
			),
			array(
				'id' => $prefix . 'map_6',
				'type' => 'map',
				'name' => esc_html__( 'Map', 'my_theme' ),
				'address_field' => 'map_6',
				'region' => 'Nepal',
			),
			array(
				'id' => $prefix . 'video_7',
				'type' => 'video',
				'name' => esc_html__( 'Video', 'my_theme' ),
				'max_file_uploads' => 4,
			),
			array(
				'id' => $prefix . 'image_advanced_8',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Image Advanced', 'my_theme' ),
			),
			array(
			    'id'   => 'background',
			    'name' => 'Background',
			    'type' => 'background',
			),

		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'custom_meta_box' );